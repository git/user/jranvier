# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit git-2

DESCRIPTION="Armory is an advanced Bitcoin client"
HOMEPAGE="http://bitcoinarmory.com/"

EGIT_REPO_URI="git://github.com/etotheipi/BitcoinArmory.git"
EGIT_COMMIT="v0.87.2-beta"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"

RDEPEND="net-p2p/bitcoin-qt"

DEPEND="dev-python/PyQt4
	dev-lang/swig
	dev-python/twisted
	x11-misc/xdg-utils"

src_install() {
	emake DESTDIR="${D}" install
	dodoc README
}

pkg_postinst() {
	xdg-icon-resource install --novendor --context apps --size 64 /usr/share/armory/img/armory_icon_64x64.png armoryicon
	xdg-icon-resource install --novendor --context apps --size 64 /usr/share/armory/img/armory_icon_64x64.png armoryofflineicon
	xdg-icon-resource install --novendor --context apps --size 64 /usr/share/armory/img/armory_icon_green_64x64.png armorytestneticon
}
